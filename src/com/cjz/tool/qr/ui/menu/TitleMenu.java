package com.cjz.tool.qr.ui.menu;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JMenuItem;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import com.cjz.tool.qr.QRreader;
import com.cjz.tool.qr.config.Setting;
import com.cjz.tool.qr.ui.AboutDialog;
import com.cjz.tool.qr.ui.MainFrame;

import craky.componentc.JCMenu;
import craky.componentc.JCMenuItem;
import craky.util.UIUtil;

public class TitleMenu extends JCMenu implements ActionListener, PopupMenuListener {

	private static final long serialVersionUID = -3539338182468282071L;
	MainFrame mainFrame;

	public TitleMenu(MainFrame mainFrame) {
		super();
		this.mainFrame = mainFrame;

		setToolTipText(Setting.STR_SETTING);
		setPreferredSize(new Dimension(20, 20));
		setShowWhenRollover(false);

		getPopupMenu().addPopupMenuListener(this);
		init();
		addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				getModel().setRollover(true);
			}

			public void mouseExited(MouseEvent e) {
				getModel().setRollover(false);
			}
		});
	}

	JMenuItem copy, exit, about;

	private void init() {
		JCMenu file = new JCMenu(Setting.STR_SETTING_FILE);
		copy = new JCMenuItem(Setting.STR_SETTING_FILE_COPY_RES);
		exit = new JCMenuItem(Setting.STR_SETTING_FILE_EXIT);
		file.add(copy);
		file.addSeparator();
		file.add(exit);

		about = new JCMenuItem(Setting.STR_SETTING_ABOUT);
		add(file);
		addSeparator();
		add(mainFrame.getSettingMenuGroup());// 共用同一对象
		addSeparator();
		add(about);

		copy.addActionListener(this);
		exit.addActionListener(this);
		about.addActionListener(this);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		int width = this.getWidth();
		int height = this.getHeight();
		int iconWidth = Setting.MENU_ICON.getWidth(this);
		int iconHeight = Setting.MENU_ICON.getHeight(this);

		if (this.getModel().isSelected()) {
			UIUtil.paintImage(g, Setting.PRESSED_IMAGE, new Insets(2, 2, 2, 2), new Rectangle(0, 0, width, height), this);
		} else if (this.getModel().isRollover()) {
			UIUtil.paintImage(g, Setting.ROLLOVER_IMAGE, new Insets(2, 2, 2, 2), new Rectangle(0, 0, width, height), this);
		}
		g.drawImage(Setting.MENU_ICON, (width - iconWidth) / 2, (height - iconHeight) / 2, this);
	}

	@Override
	public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
		mainFrame.getSettingMenuGroup().setCheckState();
		// 此处因共用同一对象,该对象需重设
		add(mainFrame.getSettingMenuGroup(), 2);
	}

	@Override
	public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

	}

	@Override
	public void popupMenuCanceled(PopupMenuEvent e) {

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();

		if (source == copy) {
			QRreader.getInstance().copy2Clipboard();
		} else if (source == exit) {
			QRreader.getInstance().exit();
		} else if (source == about) {
			new AboutDialog();
		}
	}
}
